package smartpost;

import java.util.ArrayList;

public class Varasto {
    private static Varasto instance;
    private static ArrayList<Paketti> parcelsInWarehouse = new ArrayList<>();
    private static int shippedParcels = 0, receivedParcels = 0;


    private Varasto() {

    }

    //This delivery network utilizes only one warehouse. Singleton. 
    static Varasto getInstance() {
        if (instance == null) {
            instance = new Varasto();

        
        }
        return instance;
    }


    static void addParcel(Paketti parcel) {
        receivedParcels++;
        parcelsInWarehouse.add(parcel);
        System.out.println("Paketti lisätty varastoon");
        GUIController guiController = Main.getGuiController();
    }


    static void shipParcel(Paketti parcel) {
        shippedParcels++;
        GUIController guiController = Main.getGuiController();

        if (parcelsInWarehouse.contains(parcel)) {
            parcelsInWarehouse.remove(parcel);
            
        }

        guiController.prepareParcel(parcel.origin_ID, parcel.destination_ID, parcel.getParcelClass());

        if (parcel.getParcelClass() == 1 || parcel.getParcelClass() == 3) { //Fragile items in 1st & 3rd class parcels will break
            ArrayList<Esine> esineet = parcel.getParcelContents();
            for (Esine esine : esineet) {
                if (esine.getItemFragile()) {
                    esine.setItemBroken();
                    System.out.println("Hauraat esineet hajosivat.. Voivoi..");
                }
            }
        }
    }



    //Getters
    public static int getAmountOfParcels() {
        return parcelsInWarehouse.size();
    }
    public static int getShippedParcels() {return shippedParcels;}
    public static int getReceivedParcels() {return receivedParcels;}
    public static ArrayList<Paketti> getListOfParcels() {
        return parcelsInWarehouse;
    }
}