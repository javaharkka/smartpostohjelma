package smartpost;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import java.io.IOException;
import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.scene.control.ComboBox;
import javafx.scene.input.MouseEvent;

public class GUIController {
    //Create varasto
    private Varasto varasto;

    @FXML
    private WebView webView;
    @FXML
    private JFXComboBox addCities, selectParcel;
    @FXML
    private JFXButton clearRoutesButton;
    @FXML
    private JFXButton addAutomatons;
    @FXML
    private JFXButton createParcel;
    @FXML
    private JFXButton sendParcel;

    
    
    XMLParser db = new XMLParser();

    //initialize() is ran when the instance is created
	public void initialize() {
            System.out.println("toimiiimimi");
            
	    varasto = Varasto.getInstance();
            webView.getEngine().load(getClass().getResource("index.html").toExternalForm()); //Load the index.html from package, it has to be in IDE working folder
		populateComboBox(); //comboBox of cities
        
    }



    //Open a new window for creating the parcels
    @FXML
	public void createParcel() {
		try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("PakettiCreatorController.fxml")); //Open up a new page for parcel creation
            Parent root1 = fxmlLoader.load();
			Stage stage = new Stage();

			stage.setTitle("Create a new parcel");
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
		}
	}


    @FXML
	public void clearRoutes() {
		webView.getEngine().executeScript("document.deletePaths()");
	}




    //Get data from the view we created earlier, this function as a whole add's selected citys automats on to the map
    @FXML
	public void automatonsToMap() {
            String city;
            city = addCities.getValue().toString();
            for (int i = 1; i <  XMLParser.hmap.size();i++) {
                
                if(XMLParser.hmap.get(i).SPcity.equals(city)) {
                    //System.out.println(XMLParser.hmap.get(i).SPcity);
                    String postalCode = XMLParser.hmap.get(i).SPcode;
                    String address = XMLParser.hmap.get(i).SPaddress;
                    String availability = XMLParser.hmap.get(i).SPavailability;
                    String script = "document.goToLocation('" + address + " " + postalCode + " " + city + "', '" + availability + "', 'orange')";
                    webView.getEngine().executeScript(script);
                }
           
            }
        }
    @FXML
    private void selectParcelMethod(MouseEvent event) {
        System.out.println("käynnistys");
        selectParcel.getItems().clear();
        ArrayList<Paketti> allParcels = Varasto.getListOfParcels();
        if(!allParcels.isEmpty())
            System.out.println(allParcels.get(0));
            for (int i = 0; i < allParcels.size();i++) {
                selectParcel.getItems().add(allParcels.get(i));
            
        }
        }
   

    @FXML
    private void sendParcel(ActionEvent event) {
        if (selectParcel.getSelectionModel().getSelectedItem() != null) { //Get selected parcel if not null
            Paketti parcel = (Paketti) selectParcel.getSelectionModel().getSelectedItem();
            selectParcel.getSelectionModel().clearSelection(); //clear selection and add and to varasto
            Varasto.shipParcel(parcel);
        }
    }
	

    

	//Add cities to the box. 
    private void populateComboBox() {
        for (int i = 0; i < XMLParser.cities.size();i++) {
                String city = XMLParser.cities.get(i).toString();
                addCities.getItems().add(city);
        }
        
	}


    public void sendParcel() {
	    if (selectParcel.getSelectionModel().getSelectedItem() != null) { //Get selected parcel if not null
            Paketti parcel = (Paketti) selectParcel.getSelectionModel().getSelectedItem();
            selectParcel.getSelectionModel().clearSelection(); //clear selection and add and to warehouse
            Varasto.shipParcel(parcel);
        }
    }

    //This just updates every parcel on the list when the combobox is clicked.
    public void selectParcelMethod() {
        System.out.println("käynnistys");
        selectParcel.getItems().clear();
        ArrayList<Paketti> allParcels = Varasto.getListOfParcels();
        if(!allParcels.isEmpty())
            System.out.println(allParcels.get(0));
            for (int i = 0; i < allParcels.size();i++) {
                selectParcel.getItems().add(allParcels.get(i));
            
        }
    }


    //Same as drawing the map without drawing it. gets distance for calculations
    
    double getDistance(int origin_ID, int destination_ID) {
        ArrayList<String> geolocations = new ArrayList<>();

                String latitude = String.valueOf(XMLParser.hmap.get(origin_ID).SPlat);
                String longitude = String.valueOf(XMLParser.hmap.get(origin_ID).SPlng);
                geolocations.add(latitude);
                geolocations.add(longitude);
                
                latitude = String.valueOf(XMLParser.hmap.get(destination_ID).SPlat);
                longitude = String.valueOf(XMLParser.hmap.get(destination_ID).SPlng);
                geolocations.add(latitude);
                geolocations.add(longitude);
                System.out.println(geolocations.toString());

        String script = "document.getDistance(" + geolocations + ")";
        System.out.println(Double.valueOf(String.valueOf(webView.getEngine().executeScript(script))));
        return Double.valueOf(String.valueOf(webView.getEngine().executeScript(script))); //converting directly to Double is not possible. Convert to String, then double.

    }

    //Draws the routes to map.
    void prepareParcel(int origin_ID, int destination_ID, int parcelClass) {
        ArrayList<String> geolocations = new ArrayList<>();
        String script = "";
        
        
        String latitude = String.valueOf(XMLParser.hmap.get(origin_ID).SPlat);
                String longitude = String.valueOf(XMLParser.hmap.get(origin_ID).SPlng);
                geolocations.add(latitude);
                geolocations.add(longitude);
                
                latitude = String.valueOf(XMLParser.hmap.get(destination_ID).SPlat);
                longitude = String.valueOf(XMLParser.hmap.get(destination_ID).SPlng);
                geolocations.add(latitude);
                geolocations.add(longitude);
        
        

        switch (parcelClass) {
            case 1:
                script = "document.createPath(" + geolocations + ", 'red', " + parcelClass + ")";
                break;
            case 2:
                script = "document.createPath(" + geolocations + ", 'green', " + parcelClass + ")";
                break;
            case 3:
                script = "document.createPath(" + geolocations + ", 'blue', " + parcelClass + ")";
                break;
        }
        
    webView.getEngine().executeScript(script);
    }
    }