package smartpost;

import java.util.ArrayList;

class Paketti {
	private ArrayList<Esine> contents;
	private int 	parcelID = 0;
	private double 	parcelDistance;
	private double	parcelMass;
	int origin_ID;
	int destination_ID;
	int parcelClass;

	//Use counter and parcel ID to increment parcel's ID every time.
	Paketti(int origin, int destination, double mass, double distance) {
	    parcelMass      = mass;
		origin_ID       = origin;
		destination_ID  = destination;
		contents        = new ArrayList<>();
		parcelDistance  = distance;

	}


	void addItem(ArrayList<Esine> items) {
	    contents.clear(); //clear it just to be sure, this way no duplicates enter if user cancels parcel creation midway
	    contents.addAll(items);

    }




	//Getters
        ArrayList<Esine> getParcelContents() {
            return contents;
        }
        int getParcelClass() {
                        return parcelClass;
        }
        int getParcelID() {return parcelID;}
        double getParcelMass() {return parcelMass;}
	int getOrigin_ID() {return origin_ID;}
        int getDestination_ID() {return destination_ID;}
        double getParcelDistance() {return parcelDistance;}
        String parcelContentsTabSeparated() {
    	    String stringOfItems = "";
	    for (Esine item : contents) {
	        stringOfItems += "\n\tEsine: " + " " + item.getItemName() + " " + item.getItemSize() + "cm " + item.getItemMass() + "kg";
            }
            return stringOfItems;
        }
}


//Different classes
class FirstClassParcel extends Paketti {
    FirstClassParcel(int originID, int destinationID, double mass, double distance) {
        super(originID, destinationID, mass, distance);
        parcelClass = 1;
		
    }
}

class SecondClassParcel extends Paketti {
    SecondClassParcel(int originID, int destinationID, double mass, double distance) {
        super(originID, destinationID, mass, distance);
        parcelClass = 2;
		
    }
}

class ThirdClassParcel extends Paketti {
    ThirdClassParcel(int originID, int destinationID, double mass, double distance) {
        super(originID, destinationID, mass, distance);
        parcelClass = 3;
		
    }
}