package smartpost;

import com.jfoenix.controls.*;
import javafx.stage.Stage;
import javafx.scene.control.ToggleGroup;
import javafx.fxml.FXML;

import java.text.DecimalFormat;
import java.util.ArrayList;



public class PakettiCreatorController {

    
    private static final int FIRST_CLASS_HEIGHT     = 11; //centimeters
    private static final int SECOND_CLASS_HEIGHT    = 37;
    private static final int THIRD_CLASS_HEIGHT     = 59;

    private static final int PARCEL_WIDTH           = 36; 
    private static final int PARCEL_DEPTH           = 60; 

    private static final int PARCEL_MAX_MASS        = 36; //kilograms
    private static final double PARCEL_MAX_VOLUME   = 127.44; //liters

    private double parcelTotalVolume, parcelTotalMass;
    private ArrayList<Esine> tempItemList = new ArrayList<>();
    
    private ArrayList<Esine> defaultItems = new ArrayList<>();
    
    // Create esine to create defaultItems list
    String n, s;
    Double m;
    Boolean f;
    private Esine item;

	@FXML
	private JFXButton returnButton;

	@FXML
	private JFXTextField newItemNameField, newItemSizeField, newItemMassField;

	@FXML
	private JFXCheckBox newItemFragile;

	@FXML
	private JFXComboBox selectItemBox, fromCityBox, fromSmartpost, toSmartpost, toCityBox;

    @FXML
    private ToggleGroup luokkaSelection;

	@FXML
	private JFXTextArea parcelContent;
        
        
    // 
    XMLParser hm = new XMLParser();

    public void initialize() {
        parcelContent.setEditable(false); //User can't edit the content -box
        initializeItemBox(); //Populate comboboxes with entries
        initializeCityBox();
        createDefaultItems();
        luokkaSelection.selectToggle(luokkaSelection.getToggles().get(0)); //Select 1st class for the parcel automatically
        // matti kävi täällä kassu kävi täällä
    }

    private void createDefaultItems() {
            item = new Esine("Hiirimatto", "12*32*54", 0.3, false); //1 esine
            defaultItems.add(item);
            item = new Esine("Kattolamppu", "40*25*30", 1.1, true); //2
            defaultItems.add(item);
            item = new Esine("Norsu", "59*36*60", 20.0, true); //3
            defaultItems.add(item);
            item = new Esine("Puhelin", "10*15*8", 0.2, false); //4
            defaultItems.add(item);
    }


    //Populate City selection boxes with cities from database
    private void initializeCityBox() {

            for (int i = 0; i < XMLParser.cities.size();i++) {
                            String city =XMLParser.cities.get(i).toString();
                            fromCityBox.getItems().add(city);
                            toCityBox.getItems().addAll(city);
                    }     
    } 


    //Populate Item box with data from ArrayList
    public void initializeItemBox() {
        selectItemBox.getItems().clear(); //Clear the old entries before adding new


       for (int i = 0; i < defaultItems.size();i++) {
                            String name = defaultItems.get(i).getItemName();
                            String size = defaultItems.get(i).getItemSize();
                            String mass = String.valueOf(defaultItems.get(i).getItemMass());
                            selectItemBox.getItems().add(name + " " + size + "cm " + mass + "kg");
        
        
        }    
    }


    //Method below will get the Item user has selected/created, it will create an object of it, and adds it to list of temp list of items
    //It also creates database entry if necessary and adds it to the temporary list of items
	public void itemToParcel() {
            String  n = "", s = "";
            int     id = 0;
            double  m = 0;
            boolean f = false;

            //IF user has selected an item from the drop down box.
            if (selectItemBox.getSelectionModel().getSelectedItem() != null) {
                int selection = selectItemBox.getSelectionModel().getSelectedIndex();

                        n = defaultItems.get(selection).getItemName();
                        s = defaultItems.get(selection).getItemSize();
                        m = defaultItems.get(selection).getItemMass();
                        f = defaultItems.get(selection).getItemFragile();

                

                String[] temp = s.split("\\*"); //Get the items height, width and depth to calculate volume
                int h = Integer.parseInt(temp[0]);
                int w = Integer.parseInt(temp[1]);
                int d = Integer.parseInt(temp[2]);

                double volume = (h * w * d) / 1000;

                if (parcelTotalMass + m <= PARCEL_MAX_MASS && volume + parcelTotalVolume <= PARCEL_MAX_VOLUME) { //Check if it fits parcel size & mass -wise
                    Esine esine = new Esine(n, s, m, f); //Create item and add it to the list of items that will be added to parcel once user selects Class
                    tempItemList.add(esine);
                    updateParcelContent(s, n, m); //Update the content -box
                    selectItemBox.getSelectionModel().clearSelection();
                } else {
                    parcelContent.appendText("Esineesi ei mahdu minkään luokan pakettiin.\n");
                }
            }


            //IF user has created their own item
            //Takes the size input and splits it to make sure it's requirements fit those of a parcel
            //If they do, it creates an Item instance, and adds the item to the temporary list of items
            if (!newItemNameField.getText().isEmpty() && !newItemMassField.getText().isEmpty() && !newItemSizeField.getText().isEmpty()) {
                n = newItemNameField.getText();
                s = newItemSizeField.getText();

                boolean massIsDouble = true;
                try {
                    m = Double.parseDouble(newItemMassField.getText());
                } catch (Exception e) {
                    massIsDouble = false;
                }

                f = newItemFragile.isSelected();

                if (s.matches("\\d{1,2}\\*\\d{1,2}\\*\\d{1,2}") && massIsDouble && n.length() <= 128) { //Regex to get the size from the selected item, since it'll always match e.g ' 11*11*11 '
                    String[]temp = s.split("\\*");
                    int h = Integer.parseInt(temp[0]);
                    int w = Integer.parseInt(temp[1]);
                    int d = Integer.parseInt(temp[2]);
                    double volume = (h * w * d) / 1000;

                    //Make sure item fits the largest parcel.
                    if ((h <= THIRD_CLASS_HEIGHT && h > 0) && (w <= PARCEL_WIDTH && w > 0) && (d <= PARCEL_DEPTH && d > 0) && parcelTotalMass + m <= PARCEL_MAX_MASS && parcelTotalVolume + volume <= PARCEL_MAX_VOLUME) {
                        Esine esine = new Esine(n, s, m ,f); //Create the item
                        tempItemList.add(esine);
                        updateParcelContent(s, n, m);
                        newItemSizeField.clear(); newItemMassField.clear(); newItemNameField.clear(); newItemFragile.setSelected(false); //Clear the fields
                    } else {
                        parcelContent.appendText("Esineesi ei mahdu minkään luokan pakettiin.\n");
                    }
                } else {
                    parcelContent.appendText("Tarkista syöte.\n");
                }
            }
    }


    //method to add text to parcel content box
    private void updateParcelContent(String stringToParse, String name, double mass) {
        String[] temp = stringToParse.split("\\*");
        int h = Integer.parseInt(temp[0]);
        int w = Integer.parseInt(temp[1]);
        int d = Integer.parseInt(temp[2]);

        parcelTotalVolume += (h * w * d) / 1000;
        parcelTotalMass += mass;

        DecimalFormat formatStyle = new DecimalFormat("##.00");
        parcelContent.appendText("Lisättiin pakettiin: " + name + " " + stringToParse + "cm "+ mass + "kg. Sisältö yht. " + formatStyle.format(parcelTotalVolume) + "L & " + formatStyle.format(parcelTotalMass) + "kg\n");
    }


    //Populate the combobox of cities with Smartpost addresses of a specific city
    public void updateFromBox() {
        fromSmartpost.getItems().clear();
        String city = fromCityBox.getValue().toString();

            for (int i = 1; i <  XMLParser.hmap.size();i++) {
                
                if(XMLParser.hmap.get(i).SPcity.equals(city)) {  
                    fromSmartpost.getItems().add(XMLParser.hmap.get(i).SPaddress);
                }
            }
    }


    //Populate the 2nd combobox with Smartpost addresses of a specific city, had to be done in 2 different methods since they're dependent on different values.
    public void updateToBox() {
        toSmartpost.getItems().clear();
        String city = toCityBox.getValue().toString();

            for (int i = 1; i <  XMLParser.hmap.size();i++) {
                
                if(XMLParser.hmap.get(i).SPcity.equals(city)) {  
                    toSmartpost.getItems().add(XMLParser.hmap.get(i).SPaddress);
                }
            }
    }
    


    //Gets parcel OriginID and DestinationID 
    public void initializeParcel() {
        int originID = 0, destinationID = 0;

        if (fromSmartpost.getSelectionModel().getSelectedItem() != null && toSmartpost.getSelectionModel().getSelectedItem() != null) {
            String origin      = fromSmartpost.getSelectionModel().getSelectedItem().toString();
            String destination = toSmartpost.getSelectionModel().getSelectedItem().toString();
            ArrayList<SmartPost> SPList = SmartPost.getSPList();

            for (SmartPost sp : SPList) {
                if (sp.SPaddress.equals(origin)) {
                    originID = sp.SPId;
                }
                if (sp.SPaddress.equals(destination)) {
                    destinationID = sp.SPId;
                }
            }
           
            
        }

        //If user hasn't selected class, added items or selected parcel destination/, it'll notify them about it
        if (luokkaSelection.getSelectedToggle() == null || tempItemList.isEmpty() || originID == 0 || destinationID == 0) {
            parcelContent.appendText("Et ole valinnut paketin luokkaa, lähtökaupunkia, kohdetta tai pakettisi on tyhjä.\n");

        //Else if user has given needed info and all items fit, it will create the parcel in "checkItemsAndCreateParcel"
        } else if (checkItemsAndCreateParcel(tempItemList, originID, destinationID)) {
            closeWindow(); //Close the window once parcel has been created
        } else {
            parcelContent.appendText("Tavarasi eivät mahdu nykyisen luokan kuljetukseen tai yrität lähettää 1lk pakettia liian kauas!\n");
        }
    }


    /*check if all parcel info is in the right limits of the selected class.
     */

    private boolean checkItemsAndCreateParcel(ArrayList<Esine> tempItemList, int originID, int destinationID) {
        boolean itemsFit = true;
        JFXToggleButton selectedClass = (JFXToggleButton)luokkaSelection.getSelectedToggle();
        GUIController guiController = Main.getGuiController(); //Get the GUIController object, so we can use its method "createPath" and get the distance parcel has to travel.
        Double distance = guiController.getDistance(originID, destinationID); //Check the distance of 1st class parcel here before creation already


        switch (selectedClass.getText()) {
            case "1. Luokka":

                if (distance > 150) {
                    return false;
                }

                for (Esine esine : tempItemList) { //Loop through the temporary list of items to make sure they all fit the parcel's class
                    if (esine.getItemHeight() > FIRST_CLASS_HEIGHT) {
                        itemsFit = false;
                    }
                }

                if (itemsFit) { //If all is good, it is going to create the Parcel object and add the items from templist to the parcel
                    FirstClassParcel firstClassParcel = new FirstClassParcel(originID, destinationID, parcelTotalMass, distance);
                    firstClassParcel.addItem(tempItemList);
                    Varasto.addParcel(firstClassParcel); //And add the parcel to warehouse
                }
                break;

            case "2. Luokka":
                for (Esine esine : tempItemList) {
                    if (esine.getItemHeight() > SECOND_CLASS_HEIGHT) {
                        itemsFit = false;
                    }
                }
                if (itemsFit) {
                    SecondClassParcel secondClassParcel = new SecondClassParcel(originID, destinationID, parcelTotalMass, distance);
                    secondClassParcel.addItem(tempItemList);
                    Varasto.addParcel(secondClassParcel);
                }
                break;

            case "3. Luokka":
                ThirdClassParcel thirdClassParcel = new ThirdClassParcel(originID, destinationID, parcelTotalMass, distance);
                //We dont need to perform other checks as all items will always fit the third class
                thirdClassParcel.addItem(tempItemList);
                Varasto.addParcel(thirdClassParcel);
                break; 
            }
        return itemsFit;
    }


    //Clear users Item -selection
    public void clearItemSelection() {
        selectItemBox.getSelectionModel().clearSelection();
    }


    //Close the window
    public void closeWindow() {
        Stage stage = (Stage) returnButton.getScene().getWindow();
        stage.close();
    }
}