package smartpost;

import java.util.ArrayList;

class SmartPost {
    int SPId;
	String SPcode, SPcity, SPaddress, SPavailability, SPpostoffice;
	double SPlat, SPlng;
	private static ArrayList<SmartPost> SPList = new ArrayList<>();

	SmartPost(int id, String code, String city, String address, String availability, String postoffice, double lat, double lng) {
		SPId      = id;
                SPcode    = code;
		SPcity    = city;
		SPaddress = address;
		SPavailability = availability;
		SPpostoffice   = postoffice;
		SPlat     = lat;
		SPlng     = lng;
		SPList.add(this);
	}

	static ArrayList<SmartPost> getSPList() {
	    return SPList;
    }
}