
package smartpost;


public class Esine {
        
    private String name;
    private String size;
    private double mass;
    private boolean fragile;
    private boolean broken;

 
Esine(String itemName, String itemSize, double itemMass, boolean itemFragile) {
        name = itemName;
        size = itemSize;
        mass = itemMass;
        fragile = itemFragile;
        broken  = false;

    }

 //Used to make sure the item's height is within the parcel's limits.
    int getItemHeight() {
        String[] temp = size.split("\\*");
        return Integer.parseInt(temp[0]); //size is string such as 1*2*3, where height would be 1.
    }

    //Getters
    String getItemName() {
        return name;
    }

    String getItemSize() {
        return size;
    }
    double getItemMass() {
        return mass;
    }
    boolean getItemFragile() {
        return fragile;
    }  
    
    //Setter
    void setItemBroken() {broken = true;}
}