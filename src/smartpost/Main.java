/**
 * Created by Kasper Koskenvirta 0452185, Matvei Tikka 0453100
 * 
 *
 * This GUI uses JFoenix material design library https://github.com/jfoenixadmin/JFoenix
 *
 * 
 */

package smartpost;

import java.io.IOException;
import java.util.HashMap;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
	private static GUIController guiController; //Save the instance of GUIController so ParcelCreatorController can use the getter fucntion.
	//ParcelCreatorController needs methods in GUIController to check the distance

	@Override
	public void start(Stage primaryStage) throws IOException {
		//Set up the GUI

			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/smartpost/GUI.fxml"));
			Parent root = fxmlLoader.load();
			guiController = fxmlLoader.getController(); //Cast to (GUIController) if needed


			Scene scene = new Scene(root);

			primaryStage.setScene(scene);
			primaryStage.show();
	}


	static GUIController getGuiController() {
		return guiController;
	}


	public static void main(String[] args) {

                //Parse the xml file
                XMLParser.parseData();

		//Initialize warehouse/varasto. We use singleton pattern here because we only have one warehouse
		Varasto varasto = Varasto.getInstance();

		//UI launch
		launch(args);
	}

}

